---
menu:
  main:
    name: Instalatu
    weight: 4
sassFiles:
- scss/get.scss
title: Plasma Mobile eskaintzen duten banaketak
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM, ARM gailuetarako Manjaro banaketa da. Arch Linux ARM-en oinarrituta dago, Manjaro tresnak, gaiak eta azpiegiturarekin konbinatuta, zure ARM gailuetarako irudiak instalatzeko.

[Webgunea](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Zama-jaitsi:

* [Azken egonkorra (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Garatzaileentzako eraikinak] (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalazioa

PinePhone-rako, informazio generikoa aurki dezakezu [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)an.

### openSUSE

![](/img/openSUSE.svg)

openSUSE, behinola SUSE Linux eta SuSE Linux Professional, SUSE Linux GmbH-ek eta beste konpainia batzuek babestutako Linux banaketa bat da. Gaur egun, openSUSE-k Tumbleweed oinarri duten Plasma Mobile eraikinak hornitzen ditu.

##### Zama-jaitsi

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instalazioa

PinePhone-rako, informazio generikoa aurki dezakezu [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)an.

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), telefono adimendunetan eta beste gailu mugikorretan instalatu daitekeen, ukimenerako optimizatutako, aurre-konfiguratutako Alpine Linux bat da. Begiratu [gailu-zerrenda](https://wiki.postmarketos.org/wiki/Devices) zure gailuak euskarria izateko aurrerapena ikusteko.

Aurre-eraikitako irudirik ez duten gailuetarako, «pmbootstrap» erabili beharko duzu irudia flash memorian eskuz grabatzeko. Jarraitu [honako](https://wiki.postmarketos.org/wiki/Installation_guide) jarraibideak. Egiaztatu gailuaren wiki orria funtzionatzen ari denari buruzko informazio gehiago lortzeko.

[Jakizu gehiago](https://postmarketos.org)

##### Zama-jaitsi:

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [«Edge» berriena (PinePhone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Komunitateko gailuak](https://postmarketos.org/download/)

### Neon oinarriko erreferentzia «rootfs»

![](/img/neon.svg)

KDE Neon-en oinarritutako irudia. KDE Neon bera Ubuntu 20.04ean (Focal) oinarrituta dago. KDE Neon-en garapen ezegonkorreko («dev-unstable») adarrean oinarrituta dago, eta beti kaleratzen ditu git nagusiko KDE Frameworks azpiegituren, KWin eta Plasma Mobile-ren bertsio berrienak.

Une honetan ez dago irudiak mantentzeko lanik martxan.

##### Zama-jaitsi:

* [PinePhone](https://images.plasma-mobile.org/pinephone/)

## Instalazioa

Irudia zama-jaitsi, deskonprimatu eta grabatu SD-txartel batean «dd» edo tresna grafiko bat erabiliz. PinePhone automatikoki abiatuko da SD-txartel batetik. Barneko flash-ean instalatzeko, mesedez, jarraitu [Pine wikiko](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions) jarraibideak.

## Mahaigaineko gailuak

### Neon oinarriko amd64 ISO irudia

![](/img/neon.svg)

ISO irudi honek Neon-en oinarritutako erreferentziako «rootfs»ek erabiltzen dituen pakete berdinak erabiltzen ditu. Android-ez darabilten intel tabletetan, PCetan eta alegiazko makinetan erabil daiteke.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
