---
author: Plasma Mobile team
created_at: 2020-12-01 11:40:00 UTC
date: "2020-12-01T00:00:00Z"
title: 'PinePhone: KDE community edition'
---

[PinePhone KDE Community edition](https://kde.org/announcements/pinephone-plasma-mobile-edition/) pre-orders start from today, and we are continuously working on polishing the software that will be shipped with it.

Plasma Mobile is a user interface for phones and tablets, just like Plasma Desktop is for notebooks and desktop PCs.

There are various distributions that package Plasma Mobile,

- [KDE Neon](https://neon.kde.org)
- [Manjaro ARM](https://manjaro.org)
- [postmarketOS](https://postmarketos.org)

Although there is some overlap between the Plasma Mobile and the KDE Neon team, Plasma Mobile is not tied to KDE Neon.

The Manjaro team has experience with the PinePhone hardware from building their images for the PinePhone shipping with Phosh. To provide the best Plasma Mobile experience, we decided to build on top of their work.

The KDE Community PinePhone will come with Plasma Mobile running on a customized installation of Manjaro ARM, giving you access to mobile applications from other environments already packaged in Manjaro, and the proven hardware support known from other Manjaro images.

If you would like to see a preview of what will be shipped with PinePhone KDE Community edition. You can download [unstable nightly image from their website](https://kdebuild.manjaro.org/images/dev/)

We also aim to work with the KDE Neon and postmarketOS teams in the future to improve the support for Plasma Mobile in their distributions. This will ensure that you will be able to use Plasma Mobile in your favorite distribution.
